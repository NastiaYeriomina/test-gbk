var gulp = require('gulp');
var watch = require('gulp-watch');
var browserSync = require('browser-sync');
// var reload = browserSync.reload;


//define pathes
var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: {
        template: 'src/template/index.pug',
        js: 'src/js/main.js',
        scss: 'src/scss/main.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        template: 'src/**/*.pug',
        js: 'src/js/**/*.js',
        scss: 'src/scss/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: 'build'
};


//pug task
var pug = require('gulp-pug');
var prettify = require('gulp-prettify');

gulp.task('pug', function buildHTML() {
  return gulp.src(path.src.template)
  .pipe(pug())
  .pipe(prettify({
      indent_size: 2,
      wrap_attributes: 'auto',
      preserve_newlines: true,
      end_with_newline: true
  }))
  .pipe(gulp.dest('build/'));
});
gulp.task('pug:watch', function(){
  gulp.watch([path.watch.template], ['pug']);
});


//scss task
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var mqpacker = require('css-mqpacker');
var cssmin = require('gulp-minify-css');

function isMax(mq) {
    return /max-width/.test(mq);
}
function isMin(mq) {
    return /min-width/.test(mq);
}
function sortMediaQueries(a, b) {
    A = a.replace(/\D/g, '');
    B = b.replace(/\D/g, '');

    if (isMax(a) && isMax(b)) {
        return B - A;
    } else if (isMin(a) && isMin(b)) {
        return A - B;
    } else if (isMax(a) && isMin(b)) {
        return 1;
    } else if (isMin(a) && isMax(b)) {
        return -1;
    }
    return 1;
}

gulp.task('sass', function(){
  return gulp.src(path.src.scss)
  .pipe(sourcemaps.init())
  .pipe(sass())
  .pipe(postcss([
      autoprefixer({
          browsers: ['last 4 versions'],
          cascade: false
        }),
        mqpacker({
          sort: sortMediaQueries
        })
      ]))
  .pipe(cssmin())
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(path.build.css));
});
gulp.task('sass:watch', function(){
  gulp.watch([path.watch.scss], ['sass']);
});


//js task
var babel = require('gulp-babel');
var include = require('gulp-include');

gulp.task('js', function(){
  return gulp.src(path.src.js)
  .pipe(include())
  .pipe(babel())
  .pipe(gulp.dest(path.build.js));
});
gulp.task('js:watch', function(){
  gulp.watch([path.watch.js], ['js']);
});


//copy tasks
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

gulp.task('img', function(){
  return gulp.src(path.src.img)
  .pipe(imagemin({
    progressive: true,
    use: [pngquant()],
    interlaced: true
  }))
  .pipe(gulp.dest(path.build.img));
});

gulp.task('fonts', function(){
  return gulp.src(path.src.fonts)
  .pipe(gulp.dest(path.build.fonts));
});

gulp.task('copy',[
  'img',
  'fonts'
]);
gulp.task('copy:watch', function(){
  gulp.watch([path.watch.img], ['copy']);
});


//watch task
gulp.task('watch', [
  'copy:watch',
  'pug:watch',
  'js:watch',
  'sass:watch'
]);


//server config & task
var config = {
    server: {
        baseDir: "./build"
    },
    files: [
        path.build.html,
        path.build.css,
        path.build.js,
        path.build.img
    ],
    notify: false,
    tunnel: true,
    online: false,
    open: false,
    host: 'localhost',
    port: 8080
};
gulp.task('server', function () {
    browserSync.create().init(config);
});


//clean task
var del = require('del');
gulp.task('clean', function(){
  return del.sync(path.clean);
});


//build task
var runSequence = require('run-sequence');

function build(cb) {
  runSequence(
    'clean',
    'sass',
    'pug',
    'js',
    'copy',
    cb
  );
}
gulp.task('build', function(cb) {
    build(cb);
});
// gulp.task('build', [
//   'clean',
//   'sass',
//   'pug',
//   'js',
//   'copy'
// ]);


//default task
gulp.task('default', [
  'build',
  'server',
  'watch'
]);
