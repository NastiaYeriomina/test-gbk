# Project Title

This is a test task for GBKSoft. A timetable with tasks for teams.

#How to use

Clone this repo and then in command line type:

* `npm install` - install all dependencies
* `gulp` - build project, run dev-server and let magic happen

## Authors

* **Anastasia Yeriomina** - *Initial work* - [NastiaYeriomina](https://bitbucket.org/NastiaYeriomina)
