//date slider init
  $('#date-slider').slider({
    range: true,
    min: new Date('2016/01/01').getTime() / 1000, //in seconds
    max: new Date('2018/01/01').getTime() / 1000,
    step: 86400, //1 day
    values: [
      new Date('2016/01/01').getTime() / 1000,
      new Date('2017/11/01').getTime() / 1000
    ]
  });
  $( "#date-slider" ).on( "slidechange", function( event, ui ){
    var values = $(this).slider( "values" );
    var minText = $('.js-date-min').text();
    var maxText = $('.js-date-max').text();
    var newMin = new Date(values[0] * 1000).toDateString().substr(4).split(' ');
    var newMax = new Date(values[1] * 1000).toDateString().substr(4).split(' ');
    newMin[1] = newMin[1] + ',';
    newMax[1] = newMax[1] + ',';
    if(newMin[1].slice(0,1) == 0){
      newMin[1] = newMin[1].slice(1);
    }
    if(newMax[1].slice(0,1) == 0){
      newMax[1] = newMax[1].slice(1);
    }
    newMin = newMin.join(' ');
    newMax = newMax.join(' ');
    if(minText != newMin){
      $('.js-date-min').text(newMin);
    }
    if(maxText != newMax){
      $('.js-date-max').text(newMax);
    }

  });
